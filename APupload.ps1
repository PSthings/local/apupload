# This script enables uploading AP hash online over the internet
#Command to launch:
# powershell.exe -ExecutionPolicy Bypass -File E:\APupload.ps1

#Set level temporarily to trusted
Set-PSRepository -Name 'PSGallery' -InstallationPolicy Trusted
# Install Nuget and  Get-WindowsAutopilotinfo
Install-PackageProvider -Name NuGet -Force -MinimumVersion 2.8.5.201 -Scope CurrentUser -Verbose -ErrorAction Stop
Install-Script Get-WindowsAutoPilotInfo -Force -Confirm:$false -Verbose -Scope CurrentUser -ErrorAction Stop
if ($?){
    Write-Host "Get-WindowsAutoPilotInfo Installed. Wait for MS login window..." -ForegroundColor Green
}
#Set level back to default : unstrusted
Set-PSRepository -Name 'PSGallery' -InstallationPolicy Untrusted
# Run Get-WindowsAutopilotinfo online
Get-WindowsAutopilotinfo -Online